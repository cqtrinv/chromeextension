# Time-stamp: "2023-04-21 18:19:07 queinnec"
# Cf. https://developer.chrome.com/extensions/overview

work : zip
clean :: cleanMakefile

zip : logo-128x128.png favicon.png
	git status .
	if git status . | grep -Eq '(not staged|Untracked)' ; then \
		echo "*** Git not committed!" ; exit 1 ; else true ; fi
	rm -f ../TrinvChromeExtension*.zip || true
	node mkzip.js
	export VERSION=`jq .version manifest.json | tr -d '"'` ; \
	  git tag -a "$$VERSION" -m "version $$VERSION"
# So it may be uploaded to the Chrome Extension Webstore:
	cp -fp ../TrinvChromeExtension4Firefox.zip ../FirefoxHOME/Desktop/

logo-128x128.png : ../Logo/logo-512x512.png
	convert -geometry 128x128 \
		../Logo/logo-512x512.png \
		logo-128x128.png
favicon.png : ../Logo/logo-512x512.png
	convert -geometry 32x32 \
		../Logo/logo-512x512.png \
		favicon.png

# Test: rebuild that zip without version incrementation:
../TrinvChromeExtension4Firefox.zip :
	zip ../TrinvChromeExtension4Firefox.zip \
		manifest.json favicon.png logo-128x128.png \
		popup.css popup.html popup.js

# ###################### Only useful for self-hosting:
../TrinvChromeExtension.pem :
	-rm -f ../TrinvChromeExtension.key*
	ssh-keygen -t rsa -b 2048 -m PEM -N '' \
		-C "TrinvChromeExtension" -f ../TrinvChromeExtension.key
	openssl pkcs8 -topk8 --inform PEM -outform PEM -nocrypt \
		-in ../TrinvChromeExtension.key -out ../TrinvChromeExtension.pem
	rm -f ../TrinvChromeExtension.key*

# end of Makefile
