/** Utility to pack a Chrome extension, 
    it also increment the current version. 
*/

const fs = require('fs');
const path = require('path');

const semver = require(`semver`);
const JSZip = require('jszip');

const name = 'TrinvChromeExtension';
const pwd = path.resolve(`../${name}`);

// Transmit version from package.json towards manifest.json:
const pkg = require(`${pwd}/package.json`);
const manifest = require(`${pwd}/manifest.json`);
const version = semver.inc(pkg.version, 'patch');
manifest.version = version;
//console.log(JSON.stringify(manifest, null, 4));
fs.writeFileSync(`${pwd}/manifest.json`,
                 JSON.stringify(manifest, null, 4) + '\n');
pkg.version = version;
fs.writeFileSync(`${pwd}/package.json`,
                 JSON.stringify(pkg, null, 4) + '\n');

const files = [
	"favicon.png",
	//"manifest.json",  // special treatment for Firefox
	"logo-128x128.png",
	"popup.css",
	"popup.html",
    "popup.js"
];

function generateZipForChrome () {
    console.log("*** Prepare zip archive for Chrome,Brave,Edge,Opera...");
    const zip = new JSZip();
    zip.file('manifest.json', JSON.stringify(manifest, null, 4));
    files.forEach(file => {
        console.log(`zipping ${file}...`);
        const content = fs.readFileSync(`./${file}`);
        //console.log(`zipped ${file} - ${content.length} bytes ...`);
        zip.file(file, content);
    });
    return zip.generateAsync({
        type: "array",
        compression: "DEFLATE",
        compressionOptions: {
            level: 9
        }
    }).then(content => {
        fs.writeFileSync(`${pwd}4Chrome.zip`, Buffer.from(content));
    }).catch(error => {
        console.error('********zip error', error);
        process.exit(1);
    });
}

function generateZipForFirefox () {
    console.log("*** Prepare zip archive for Firefox...");
    manifest.browser_specific_settings = {
        "gecko": {
            "id": "trinvext@trinv.fr",
            "strict_min_version": "109.0"
        }
    };
    const zip = new JSZip();
    zip.file('manifest.json', JSON.stringify(manifest, null, 4));
    files.forEach(file => {
        console.log(`zipping ${file}...`);
        const content = fs.readFileSync(`./${file}`);
        //console.log(`zipped ${file} - ${content.length} bytes ...`);
        zip.file(file, content);
    });
    return zip.generateAsync({
        type: "array",
        compression: "DEFLATE",
        compressionOptions: {
            level: 9
        }
    }).then(content => {
        fs.writeFileSync(`${pwd}4Firefox.zip`, Buffer.from(content));
    }).catch(error => {
        console.error('********zip error', error);
        process.exit(2);
    });
}

function generateZips () {
    return generateZipForChrome()
        .then(() => {
            return generateZipForFirefox();
        }).then(() => {
            process.exit(0);
        })
}
generateZips();

    

// end of mkzip.js
