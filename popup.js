/**
   TrinvExt - a Chrome extension to ease TRINV (cadasTRe INVersé) to manage
   real estate advertisements.
   
   The code is rather simple, it just reads the HTML content current page
   and sends it to the TRINV server. This server will extract the main
   characteristics of the advertisement and return a JSON descriptor
   to be used by TRINV client.

 */

// popup.js adds event handlers to popup.html

const analyzeButton = document.getElementById('analyze');

async function getCurrentTab() {
    let queryOptions = { active: true, currentWindow: true };
    let [tab] = await chrome.tabs.query(queryOptions);
    return tab;
}

function grabPage () {
    
    const baseURL = 'https://trinv.fr';
    const scraperURL = baseURL + '/content';
    
    /**
       Useful for debug. Display what happens in the console.

       @eturns {nothing}
    */

    function log (...args) {
        console.log('TRINVext', ...args);
        //alert(args.join(', '));
    }

    /**
       Get the HTML content of the page. Add the current URL and
       current title as HTML comments in front of the payload.
       
       @return {Object} - main characteristics
       @return {Object.url} -- URLencoded url
       @return {Object.title} -- URLencoded title
       @return {Object.content} -- HTML content
    */

    function getPageContent (document) {
        let url = encodeURIComponent(document.URL);
        let title = encodeURIComponent(document.title);
        let content = `<!-- documentURL="${url}" -->\n`;
        content += `<!-- documentTitle="${title}" -->\n`;
        //content += document.body.innerHTML;
        // NOTA get <head> and <body>:
        content += document.documentElement.innerHTML;
        log(`HTML read: ${url}`);
        return { url, title, content };
    }

    /** 
        Always post the HTML content page. The result is a JSON object
        describing what is known about this house.
        
        @return Promise(json)
    */

    function postPage (content) {
        return window.fetch(`${scraperURL}/`, {
            method: 'POST',
            mode: 'cors',
            //mode: 'no-cors',
            headers: {
                'X-trinv': document.location,
                'Content-Type': 'text/plain',
                'Accept': 'application/json'
            },
            body: content
        }).then((response) => {
            log('POST status: ' + response.status);
            return response.json();
        });
    }

    log('Start analysis...');
    const { url, title, content } = getPageContent(window.document);
    return postPage(content)
        .then(json => {
            if ( json.uuid ) {
                log("End analysis.");
                let apiurl = `${baseURL}/fiche/${json.uuid}`;
                window.open(apiurl, "_blank");
            } else {
                log('Analysis problem: ' + JSON.stringify(json));
                throw new Error(json.error);
            }
        })
        .catch(exc => {
            log("Bad analysis", exc);
            let apiurl = `${baseURL}/nouvelle-fiche?url=${url}&title=${title}`;
            window.open(apiurl, "_blank");
        });
}

analyzeButton.addEventListener('click', async () => {
    let tab = await getCurrentTab();
    chrome.scripting.executeScript({
        target: {tabId: tab.id},
        func: grabPage
    }).then(() => {
        // Close the extension popup (needed for Firefox):
        window.close();
    });
});

// end of popup.js
