# TrinvExt une extension à Chrome

TrinvExt est une extension à Chrome (valable aussi pour
Edge, Brave, Opera et Firefox) simplifiant la mise en oeuvre de
[TRINV](https://trinv.fr/). TRINV est une application permettant de
visualiser les parcelles cadastrales d'une surface donnée dans une
commune donnée. TRINV permet aussi de mémoriser des annonces
immobilières, de les classer, de les annoter de manière à organiser
vos recherches d'un bien immobilier.

<img src="Images/fullrecords.png"
     style="display:block;margin: auto;border: 1px solid #dfdfdf;border-radius: .5rem;padding: .25rem;height: 200px;"
     alt="exemples d'annonces" >

TRINV mémorise une annonce immobilière en extrayant ses principales
caractéristiques. Pour cela, l'IA de TRINV a besoin de lire puis
d'analyser la page de cette annonce ce que permet TrinvChromeExtension
sans autre effort qu'un simple clic.

Comme ajouter n'importe quelle extension à son navigateur est une
opération qui peut comporter des risques, le
[code de cette extension](https://gitlab.com/cqtrinv/chromeextension)
est public. Cette extension ne modifie rien dans la page courante et
se contente de la lire.

## Installation

### Pour Brave, Chrome, Edge, Opera

Pour installer TrinvExt, il faut chercher dans le
[magasin des extensions Chrome](https://chrome.google.com/webstore/)
et cliquer sur « Ajouter à Chrome »:

<img src="Images/extension1.png"
     style="height: 200px;margin: auto;border: 1px solid #dfdfdf;border-radius: .5rem;padding: .25rem;"
     alt="installation de TrinvExt - étape 1" >

Puis cliquer sur « Ajout de l'extension »:

<img src="Images/extension2.png"
     style="height: 200px;margin: auto;border: 1px solid #dfdfdf;border-radius: .5rem;padding: .25rem;"
     alt="installation de TrinvExt - étape 2" >

C'est fait, l'extension est installée.

### Pour Firefox

Pour installer TrinvExt sur Firefox, il faut aller la chercher dans le
[magasin des extensions Firefox](https://addons.mozilla.org/en-US/firefox/addon/trinvext/) et cliquer « ajouter à Firefox ».

## Mode d'emploi

Cliquer sur l'icône de TrinvExt puis choisissez le bouton « Analyser
». Si vous ne voyez pas l'icône, cherchez l'icône des extensions (une
pièce de puzzle) et épinglez TrinvExt afin qu'elle soit toujours
apparente.

<img src="Images/extension3.png"
     style="height: 200px;margin: auto;border: 1px solid #dfdfdf;border-radius: .5rem;padding: .25rem;"
     alt="installation de TrinvExt - étape 3" >

Que l'analyse soit fructueuse ou pas, un nouvel onglet s'ouvrira avec
la fiche associée à l'annonce. Vous pourrez rectifier ou compléter 
cette fiche à votre guise.

Attention, sur Firefox, le nouvel onglet ne s'ouvrira que si vous
l'autorisez. Un bandeau horizontal apparaît en haut, cliquez sur le
bouton « Preferences » et choisissez « ouvrir
https://trinv.fr/fiche/... ».

<img src="Images/firefox-showTab1.png"
     style="height: 200px;"
     alt="Usage de TrinvExt pour Firefox - étape 1" >
<img src="Images/firefox-showTab3.png"
     style="height: 200px;"
     alt="Usage de TrinvExt pour Firefox - étape 2" >


## Explication du code

Le [code de cette extension](https://gitlab.com/cqtrinv/chromeextension)
est public.

Le fichier `package.json` indique les fichiers présents dans l'extension. 
Il contient ce qui est nécessaire pour la regénération de l'extension.

Le seul fichier exécutable et vraiment intéressant est `popup.js` qui
capture le code de la page contenant l'annonce et l'envoie à l'IA de
TRINV pour analyse. Si l'analyse se passe bien, la fiche engendrée
apparaîtra dans un nouvel onglet sinon une fiche à compléter sera
néanmoins créée.


