/**
   TrinvExt is a Chrome extension to ease TRINV (cadasTRe INVersé) to
   manage real estate advertisements. This code just tells TRINV that
   the TRINV extension is installed.

   The installation is marked by adding a classname to the <HTML> tag.
   This is done only when accessing https://trinv.fr/ so the TRINV
   server may advise the user to analyze real estate advertisements
   with the extension for better results.
*/

const htmls = document.getElementsByTagName('html');

if ( htmls.length > 0 ) {
    const html = htmls.item(0);
    html.classList.add('_TrinvExtInstalled');
    console.log('TrinvExt is installed');
}

// end of iAmHere.js
